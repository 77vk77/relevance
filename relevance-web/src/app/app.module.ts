import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { FormsModule}      from '@angular/forms';
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import { EmployeeComponent } from './employee/employee.component';
import { FormComponent } from './form/form.component';
import { CalendarComponent } from './calendar/calendar.component';
import { ConnectComponent } from './connect/connect.component';
import { NewsComponent } from './news/news.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FoldersComponent } from './folders/folders.component';
import { QuickComponent } from './quick/quick.component';
import { MyDetailsComponent } from './my-details/my-details.component';
import { BirthdayComponent } from './birthday/birthday.component';
import { EmployeeDirecComponent } from './employee-direc/employee-direc.component';
import { GalleryComponent } from './gallery/gallery.component';
import { RewardsComponent } from './rewards/rewards.component';
import { NewsletterComponent } from './newsletter/newsletter.component';
import {ProgressBarModule} from "angular-progress-bar";
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { FormDetailsComponent } from './form-details/form-details.component';
import { NewsletterDetailsComponent } from './newsletter-details/newsletter-details.component';
import { PolicyDetailsComponent } from './policy-details/policy-details.component';
import { ItPoliciesComponent } from './it-policies/it-policies.component';
import { OrganisationStructureComponent } from './organisation-structure/organisation-structure.component';
import { HrPoliciesComponent } from './hr-policies/hr-policies.component';
import { ApplyLeaveComponent } from './apply-leave/apply-leave.component';
import { GeneralInfoComponent } from './general-info/general-info.component';
import { RaiseQueryComponent } from './raise-query/raise-query.component';
import { SettingsComponent } from './settings/settings.component'


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    SignupComponent,
    EmployeeComponent,
    FormComponent,
    CalendarComponent,
    ConnectComponent,
    NewsComponent,
    FoldersComponent,
    QuickComponent,
    MyDetailsComponent,
    BirthdayComponent,
    EmployeeDirecComponent,
    GalleryComponent,
    RewardsComponent,
    NewsletterComponent,
    ProgressBarComponent,
    FormDetailsComponent,
    NewsletterDetailsComponent,
    PolicyDetailsComponent,
    ItPoliciesComponent,
    OrganisationStructureComponent,
    HrPoliciesComponent,
    ApplyLeaveComponent,
    GeneralInfoComponent,
    RaiseQueryComponent,
    SettingsComponent, 
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    ProgressBarModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
