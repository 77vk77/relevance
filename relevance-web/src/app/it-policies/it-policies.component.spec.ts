import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItPoliciesComponent } from './it-policies.component';

describe('ItPoliciesComponent', () => {
  let component: ItPoliciesComponent;
  let fixture: ComponentFixture<ItPoliciesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItPoliciesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItPoliciesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
