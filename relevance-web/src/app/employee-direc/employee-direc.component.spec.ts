import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeDirecComponent } from './employee-direc.component';

describe('EmployeeDirecComponent', () => {
  let component: EmployeeDirecComponent;
  let fixture: ComponentFixture<EmployeeDirecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeDirecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeDirecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
