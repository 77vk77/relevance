import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SignupComponent} from './signup/signup.component';
import {EmployeeComponent} from './employee/employee.component';
import {FormComponent} from './form/form.component';
import {BirthdayComponent} from './birthday/birthday.component';
import {EmployeeDirecComponent} from './employee-direc/employee-direc.component';
import {GalleryComponent} from './gallery/gallery.component';
import {RewardsComponent} from './rewards/rewards.component';
import {NewsletterComponent} from './newsletter/newsletter.component';
import { FormDetailsComponent } from './form-details/form-details.component';
import { NewsletterDetailsComponent} from './newsletter-details/newsletter-details.component';
import {PolicyDetailsComponent} from './policy-details/policy-details.component';
import {ItPoliciesComponent} from './it-policies/it-policies.component';
import {OrganisationStructureComponent} from './organisation-structure/organisation-structure.component';
import {HrPoliciesComponent} from './hr-policies/hr-policies.component';
import {ApplyLeaveComponent} from './apply-leave/apply-leave.component';
import {GeneralInfoComponent} from './general-info/general-info.component';
import {RaiseQueryComponent} from './raise-query/raise-query.component';
import {SettingsComponent} from './settings/settings.component';

const routes: Routes = [
  {path: 'login' , component: LoginComponent},
  {path:'employee',component: EmployeeComponent},
  {path: '' , component: NavbarComponent},
  {path: 'home' , component: NavbarComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'form', component: FormComponent},
  {path: 'birthday' , component: BirthdayComponent},
  {path: 'direct' , component: EmployeeDirecComponent},
  {path: 'gallery' , component: GalleryComponent},
  {path: 'rewards' , component: RewardsComponent},
  {path: 'newsletter' ,  component: NewsletterComponent},
  {path: 'formDetails' , component: FormDetailsComponent},
  {path: 'newsletter-details' ,  component: NewsletterDetailsComponent },
  {path: 'policy-details' ,  component: PolicyDetailsComponent},
  {path: 'it-policies' , component: ItPoliciesComponent},
  {path: 'organisation-structure' , component: OrganisationStructureComponent},
  {path: 'hr-policies' , component: HrPoliciesComponent},
  {path: 'applyLeave' , component: ApplyLeaveComponent},
  {path: 'generalInfo' , component: GeneralInfoComponent},
  {path: 'raiseQuery' , component: RaiseQueryComponent},
  {path: 'settings' , component: SettingsComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
